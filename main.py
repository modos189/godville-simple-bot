import time
import requests

def auth(data_username, data_password, data_save_login = True):
    print ("Log in to", data_username)
    r = requests.post("https://godville.net/login/login", data={'username': data_username, 'password': data_password, 'save_login': data_save_login})

    print (r.status_code)
    html = r.text
    if html.find("hero_columns") == -1:
        print ("error")
    else:
        print ("success")
    print()
    time.sleep(5)
    

def read_file(filename):
    f = open(filename)
    for line in f:
        line = line.split(":")
        auth(line[0], line[1].rstrip())
    
if __name__ == "__main__":
    read_file("accounts.txt")
